cd ~ 
echo "Make ros package using catkin_make_isolated"
cd catkin_ws
catkin_make_isolated
source devel_isolated/setup.bash

cd ~
echo "Running PX4"
gnome-terminal --tab -e 'bash -c "cd src/Firmware ; \
                                  source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default ; \
                                  no_sim=1 make px4_sitl_default gazebo ; \
                                  exec bash"'

echo "Running Gazebo Model"
gnome-terminal --tab -e 'bash -c "cd src/Firmware ; \
                                  source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default ; \
                                  roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris_modified.world ; \
                                  exec bash"'

echo "Waiting the Gazebo and PX4 initalization"
sleep 5

echo "Running Viola Jones ROS Package"
gnome-terminal --tab -e 'bash -c "cd catkin_ws ; \
                                  roslaunch opencv_apps face_detection.launch ; \
                                  exec bash"'
